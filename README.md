Edmonds Batteries. Expert battery diagnostics, selection, installation and maintenance has been saving our customers time and money since 1974. Supplying locals with their personal boat, car, RV, motorcycle, alarm and Powerwheels is just part of what we do.

Address: 33177 S Fraser Way, Abbotsford, BC V2S 2B1, Canada

Phone: 604-852-8774

Website: https://www.edmondsbatteries.com
